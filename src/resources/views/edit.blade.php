@extends('layouts.buzz')

@section('title')
  {{ 'Modifica ' . config('buzzgroups.name') }}
@endsection

@section('content')
{{ config('gruppo_api_resource') }}

  <groups-edit :group-id="{{ json_encode($gruppo->id) }}"></groups-edit>

@endsection