@extends('layouts.buzz')

@section('title')
  {{ config('buzzgroups.name') }}
@endsection

@section('content')

  <groups-index title="{{ config('buzzgroups.name') }}"></groups-index>

@endsection

