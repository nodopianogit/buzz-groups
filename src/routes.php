<?php

Route::group(['middleware' => ['web']], function() {
    Route::get('/'.config('buzzgroups.name').'/{id}', 'Nodopiano\Buzz\Groups\Controllers\GruppiController@show')->name('gruppi.show');
    Route::get('/'. config('buzzgroups.name'). '/{gruppo}/{sottogruppo}', 'Nodopiano\Buzz\Groups\Controllers\SottogruppiController@show')->name('sottogruppi.show');
});

Route::group(['prefix' => 'admin', 'middleware' => ['web','auth', 'role:admin']], function () {
    Route::get('/'.config('buzzgroups.name'), 'Nodopiano\Buzz\Groups\Controllers\GruppiController@index')->name('gruppi.admin.index');
    Route::get('/'.config('buzzgroups.name').'/crea', 'Nodopiano\Buzz\Groups\Controllers\GruppiController@create')->name('gruppi.admin.create');
    Route::post('/'.config('buzzgroups.name'), 'Nodopiano\Buzz\Groups\Controllers\GruppiController@store')->name('gruppi.admin.store');
    // Route::get('/gruppi/{id}/edit', 'Nodopiano\Buzz\Groups\Controllers\GruppiController@edit');
    Route::get('/'.config('buzzgroups.name').'/{id}', 'Nodopiano\Buzz\Groups\Controllers\GruppiController@edit')->name('gruppi.admin.edit');
    Route::post('/'.config('buzzgroups.name').'/{id}', 'Nodopiano\Buzz\Groups\Controllers\GruppiController@update')->name('gruppi.admin.update');
    Route::delete('/'.config('buzzgroups.name').'/{id}', 'Nodopiano\Buzz\Groups\Controllers\GruppiController@destroy')->name('gruppi.admin.destroy');
});

Route::group(['prefix' => 'api', 'middleware' => ['api','auth:api']], function () {
    Route::get('/'.config('buzzgroups.name').'/gerarchia', 'Nodopiano\Buzz\Groups\Controllers\Api\GerarchiaGruppiController@index');
    Route::get('/'.config('buzzgroups.name').'/{id}/gerarchia', 'Nodopiano\Buzz\Groups\Controllers\Api\GerarchiaGruppiController@show');

    Route::get('/'.config('buzzgroups.name'), 'Nodopiano\Buzz\Groups\Controllers\Api\GruppiController@index');
    Route::get('/'.config('buzzgroups.name').'/list', 'Nodopiano\Buzz\Groups\Controllers\Api\GruppiController@list');
    Route::get('/'.config('buzzgroups.name').'/{id}', 'Nodopiano\Buzz\Groups\Controllers\Api\GruppiController@show');
    Route::post('/'.config('buzzgroups.name').'/{id}', 'Nodopiano\Buzz\Groups\Controllers\Api\GruppiController@update');
});

Route::group(['prefix' => 'api', 'middleware' => ['api','auth:api', 'role:admin']], function () {
    Route::post('/'.config('buzzgroups.name'), 'Nodopiano\Buzz\Groups\Controllers\Api\GruppiController@store');
    Route::delete('/'.config('buzzgroups.name').'/{id}', 'Nodopiano\Buzz\Groups\Controllers\Api\GruppiController@destroy');
});

Route::group(['prefix' => 'api', 'middleware' => ['api','auth:api']], function () {

    $rel = config('buzzgroups.relationships');
    foreach ($rel as $nome => $model) {
        Route::get('/'.$nome.'/{id}/'.config('buzzgroups.name'),
            '\Nodopiano\Buzz\Groups\Controllers\Api\GroupableGruppiController@show')->name($nome);
        Route::get('/'.$nome.'/{id}/'.config('buzzgroups.name').'/list',
            '\Nodopiano\Buzz\Groups\Controllers\Api\GroupableGruppiController@list')->name($nome);
        Route::get('/'.config('buzzgroups.name').'/{gruppo_id}/'.$nome,
            '\Nodopiano\Buzz\Groups\Controllers\Api\GruppoGroupableController@show')->name($nome);
        Route::get('/'.config('buzzgroups.name').'/{gruppo_id}/'.$nome.'/list',
            '\Nodopiano\Buzz\Groups\Controllers\Api\GruppoGroupableController@list')->name($nome);
    }
});
