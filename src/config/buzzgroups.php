<?php

return [

/*--------------------------------
    NAME - name to be displayed into routes
        default: gruppi
---------------------------------*/
    'name' => 'gruppi',

/*--------------------------------
    REPOSITORY - repository to handle class Group
        default: 'Nodopiano\Buzz\Groups\Repositories\EloquentGroupRepository'
---------------------------------*/
    'repository' => 'Nodopiano\Buzz\Groups\Repositories\EloquentGroupRepository',

/*--------------------------------
    FORM REQUEST
        default:
            - creation: Nodopiano\Buzz\Groups\Requests\GruppoCreation::class
            - update: Nodopiano\Buzz\Groups\Requests\GruppoUpdate::class
---------------------------------*/
    'creation_request' => Nodopiano\Buzz\Groups\Requests\GruppoCreation::class,

    'update_request' => Nodopiano\Buzz\Groups\Requests\GruppoUpdate::class,

/*--------------------------------
    PAGINATE - number of results per page
        default: 15
---------------------------------*/
    'results_per_page' => 15,

/*--------------------------------
    API RESOURCE - mapping class to decorate API
        default: Nodopiano\Buzz\Groups\Resources\Gruppo::class
                 Nodopiano\Buzz\Groups\Resources\Gerarchia::class
---------------------------------*/
    'gruppo_api_resource' => Nodopiano\Buzz\Groups\Resources\Gruppo::class,
    'gerarchia_api_resource' => Nodopiano\Buzz\Groups\Resources\Gerarchia::class,

/*---------------------------------
    RELATIONSHIPS - mapping relationships into models
        default: 'users' => 'App\User'
-----------------------------------*/
    'relationships' => [
        'users' => 'App\User'
    ]

];
