<?php

namespace Nodopiano\Buzz\Groups;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

class BuzzGroupsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadViewsFrom(__DIR__.'/resources/views', 'buzz-groups');

        $this->publishes([
            __DIR__.'/config/buzzgroups.php' => config_path('buzzgroups.php')
        ], 'config');

        $this->publishes([
            __DIR__.'/resources/views' => resource_path('views/vendor/buzz-groups')
        ], 'views');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/config/buzzgroups.php', 'buzzgroups'
        );

        $this->app->bind('Nodopiano\Buzz\Groups\Repositories\GroupRepository', config('buzzgroups.repository'));

        $this->app->booting(function () {
            $loader = AliasLoader::getInstance();
            // dd(config('buzzgroups.creation_request'));
            $loader->alias('GruppoCreation', config('buzzgroups.creation_request'));
            $loader->alias('GruppoUpdate', config('buzzgroups.update_request'));
        });
    }
}
