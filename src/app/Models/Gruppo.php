<?php

namespace Nodopiano\Buzz\Groups\Models;

use Illuminate\Database\Eloquent\Model;

class Gruppo extends Model
{
    protected $table = 'gruppi';
    protected $fillable = ['name', 'parent_id'];

    public function groupable($groupable)
    {
        $model = config('buzzgroups.relationships.'.$groupable);
        return ($model == null)? collect([]) : $this->morphedByMany($model, 'groupable');
    }

    public function getDisplayedName()
    {
        return $this->name;
    }

    public function parent()
    {
        return $this->belongsTo(get_class($this), 'parent_id');
    }

    public function sottogruppi()
    {
        return $this->hasMany(get_class($this), 'parent_id');
    }

    public function isSottogruppo(Gruppo $gruppo)
    {
        return in_array($this->sottogruppi->pluck('id'), $gruppo->id);
    }

    public function isParentOf(Gruppo $gruppo)
    {
        return $this->id == $gruppo->parent_id;
    }

    public function syncRelation($relation, $items)
    {
        $res = $this->groupable($relation)->sync($items);

        foreach ($this->sottogruppi as $sottogruppo) {
            $sottogruppo->removeFromRelation($relation, $res['detached']);
        }

        if ($this->parent_id == null) {
            return;
        }

        $this->parent->addToRelation($relation, $res['attached']);
    }

    public function removeFromRelation($relation, $items)
    {
        $this->groupable($relation)->detach($items);

        foreach ($this->sottogruppi as $sottogruppo) {
            $sottogruppo->removeFromRelation($relation, $items);
        }
    }

    public function addToRelation($relation, $items)
    {
        $this->groupable($relation)->attach($items);

        if ($this->parent_id == null) {
            return;
        }

        $this->parent->addToRelation($relation, $items);
    }

    public static function gerarchiaAssoluta()
    {
        $roots = self::where('parent_id', null)->get();
        $gerarchia = [];

        foreach ($roots as $root) {
            $gerarchia[] = $root->gerarchiaRelativa();
        }

        return $gerarchia;
    }

    public function gerarchiaRelativa()
    {
        $gerarchia = $this->toArray();

        if ($this->sottogruppi->isEmpty()) {
            return $this;
        }

        $children = array();
        foreach ($this->sottogruppi as $sottogruppo) {
            $children[] = $sottogruppo->gerarchiaRelativa();
        }

        $gerarchia['children'] = $children;
        return $gerarchia;
    }
}
