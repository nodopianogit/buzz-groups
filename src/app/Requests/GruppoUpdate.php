<?php

namespace Nodopiano\Buzz\Groups\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GruppoUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'parent_id' => 'exists:gruppi,id'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => "Il nome del gruppo è un campo obbligatorio.",
            'parent_id.exists' => "Questo gruppo non esiste."
        ];
    }

    public function withValidator($validator)
    {
        $this->validateName($validator);
        $this->validateGroupable($validator);
        $this->validateParent($validator);
    }

    public function validateName($validator)
    {
        if (!isset($this->request->all()['name'])) {
            return;
        }

        $validator->after(function ($validator) {
            $gruppo = app()->make('Nodopiano\Buzz\Groups\Repositories\GroupRepository');
            $same_name = $gruppo->findByName($this->request->all()['name']);
            $exists = ($same_name->isEmpty()) ? false : $same_name->pluck('id')->first() != $this->route('id');

            if ($exists) {
                $validator->errors()->add('name', "Esiste già un gruppo con lo stesso nome.");
            }
        });
    }

    public function validateGroupable($validator)
    {
        if (! isset($this->request->all()['utenti'])) {
            return;
        }

        $validator->after(function ($validator) {
            foreach (config('buzzgroups.relationships') as $nome => $model) {
                if(isset($this->request->all()[$nome])) {
                    $items = $this->request->all()[$nome];
                    if(!is_array($items)) {
                        $items = explode(',', $items);
                        $this->offsetSet($nome, $items);
                    }
                    $esistenti = $model::whereIn('id', $items)->get();
                    foreach ($items as $i => $item) {
                        if ($esistenti->where('id', $item)->isEmpty()) {
                            $validator->errors()->add($nome . '.' . $i, "Il valore inserito per $nome non esiste.");
                        }
                    }
                }
            }
        });
    }

    public function validateParent($validator)
    {
        if (! isset($this->request->all()['parent_id'])) {
            return;
        }

        $validator->after(function ($validator) {
            $gruppo = app()->make('Nodopiano\Buzz\Groups\Repositories\GroupRepository');
            $gruppo = $gruppo->show($this->route('id'));

            if ($this->request->all()['parent_id'] != $gruppo->parent_id) {
                $validator->errors()->add('parent_id', "Impossibile modificare la gerarchia: crea un nuovo gruppo.");
            }
        });
    }
}
