<?php

namespace Nodopiano\Buzz\Groups\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GruppoCreation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'parent_id' => 'nullable|exists:gruppi,id'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => "Il nome del gruppo è un campo obbligatorio.",
            'parent_id.exists' => "Questo gruppo non esiste."
        ];
    }

    public function withValidator($validator)
    {
        $this->validateName($validator);
    }

    public function validateName($validator)
    {
        if (!isset($this->request->all()['name'])) {
            return;
        }

        $validator->after(function ($validator) {
            $gruppo = app()->make('Nodopiano\Buzz\Groups\Repositories\GroupRepository');
            $same_name = $gruppo->findByName($this->request->all()['name']);
            $exists = $same_name->isNotEmpty();

            if ($exists) {
                $validator->errors()->add('name', "Esiste già un gruppo con lo stesso nome.");
            }
        });
    }
}
