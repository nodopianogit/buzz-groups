<?php

namespace Nodopiano\Buzz\Groups\Traits;

use Illuminate\Http\Resources\Json\Resource;

trait Groupable
{
    public abstract static function apiResource();
    public abstract static function repository();
    public abstract function getDisplayedName();

    public function gruppi()
    {
        return $this->morphToMany('Nodopiano\Buzz\Groups\Models\Gruppo', 'groupable');
    }

    public function getIsGroupableAttribute()
    {
        return true;
    }

    public function syncGruppi($gruppi)
    {
        $this->gruppi()->sync($gruppi);
    }

}
