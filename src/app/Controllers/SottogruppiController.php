<?php

namespace Nodopiano\Buzz\Groups\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Nodopiano\Buzz\Groups\Models\Gruppo;
use Nodopiano\Buzz\Groups\Requests\GruppoCreation;
use Nodopiano\Buzz\Groups\Requests\GruppoUpdate;
use Nodopiano\Buzz\Groups\Repositories\GroupRepository;

class SottogruppiController extends Controller
{
    public function __construct(GroupRepository $repository)
    {
        $this->repo = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($gruppo)
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Nodopiano\Buzz\Groups\Requests\GruppoCreation  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GruppoCreation $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($gruppo, $sottogruppo)
    {
        $sottogruppo = $this->repo->show($sottogruppo);
        $gruppo = $this->repo->show($gruppo);

        if($sottogruppo->parent_id != $gruppo->id) {
            abort(404);
        }

        return view('buzz-groups::show')->with(['gruppo' => $sottogruppo]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Nodopiano\Buzz\Groups\Requests\GruppoUpdate  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GruppoUpdate $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
