<?php

namespace Nodopiano\Buzz\Groups\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use Nodopiano\Buzz\Groups\Models\Gruppo;
use Nodopiano\Buzz\Groups\Requests\GruppoCreation;
use Nodopiano\Buzz\Groups\Requests\GruppoUpdate;
use Nodopiano\Buzz\Groups\Repositories\GroupRepository;

class GruppiController extends Controller
{
    public function __construct(GroupRepository $repository)
    {
        $this->repo = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('buzz-groups::index')->with('gruppi', Gruppo::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('buzz-groups::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Nodopiano\Buzz\Groups\Requests\GruppoCreation  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GruppoCreation $request)
    {
        $this->repo->create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('buzz-groups::show')->with('gruppo', $this->repo->show($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('buzz-groups::edit')->with('gruppo', Gruppo::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Nodopiano\Buzz\Groups\Requests\GruppoUpdate  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GruppoUpdate $request, $id)
    {
        $gruppo = $this->repo->update($id, $request->all());
        return redirect()->action('\Nodopiano\Buzz\Groups\Controllers\GruppiController@edit', ['id' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repo->delete($id);
    }
}
