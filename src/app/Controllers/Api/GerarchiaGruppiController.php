<?php

namespace Nodopiano\Buzz\Groups\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Nodopiano\Buzz\Groups\Repositories\GroupRepository;

class GerarchiaGruppiController extends Controller
{
    protected $repo;

    public function __construct(GroupRepository $repository)
    {
        $this->repo = $repository;
        $this->api = config('buzzgroups.gerarchia_api_resource');
    }

    public function index()
    {
        // return Gruppo::gerarchiaAssoluta();
        return $this->api::collection($this->repo->getRoots());
    }

    public function show($id)
    {
        return new $this->api($this->repo->show($id));
    }
}