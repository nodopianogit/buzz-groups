<?php

namespace Nodopiano\Buzz\Groups\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Nodopiano\Buzz\Groups\Repositories\GroupRepository;

use GruppoCreation;
use GruppoUpdate;

class GruppiController extends Controller
{
    protected $repo;
    protected $api;

    public function __construct(GroupRepository $repository)
    {
        $this->repo = $repository;
        $this->api = config('buzzgroups.gruppo_api_resource');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->api::collection($this->repo->filter('name', $request->filter)->parent($request->parent)
                          ->sort($request->sort)->paginate(config('buzzgroups.results_per_page')));
    }

    public function list()
    {
        return $this->repo->list();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GruppoCreation $request)
    {
        return $this->repo->create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new $this->api($this->repo->show($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GruppoUpdate $request, $id)
    {
        return $this->repo->update(
            $id, $request->all()
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json($this->repo->delete($id));
    }
}
