<?php

namespace Nodopiano\Buzz\Groups\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Nodopiano\Buzz\Groups\Resources\Lista;


class GroupableGruppiController extends Controller
{
    public function __construct()
    {
        $this->api = config('buzzgroups.gruppo_api_resource');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $groupable_id)
    {
        $groupable_name = $request->route()->getName();
        $groupable = config('buzzgroups.relationships.'.$groupable_name);
        if($groupable == null) {
            abort(404);
        }

        $repository = app()->make($groupable::repository());
        return $this->api::collection($repository->show($groupable_id)->gruppi()->get());
    }


    public function list(Request $request, $groupable_id)
    {
        $groupable_name = $request->route()->getName();
        $groupable = config('buzzgroups.relationships.'.$groupable_name);
        if($groupable == null) {
            abort(404);
        }

        $repository = app()->make($groupable::repository());

        return Lista::collection($repository->show($groupable_id)->gruppi()->get());
    }
}
