<?php

namespace Nodopiano\Buzz\Groups\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Nodopiano\Buzz\Groups\Repositories\GroupRepository;
use Nodopiano\Buzz\Groups\Resources\Lista;

class GruppoGroupableController extends Controller
{
    public function __construct(GroupRepository $repository)
    {
        $this->repo = $repository;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $gruppo_id)
    {
        $groupable_name = $request->route()->getName();
        $gruppo = $this->repo->show($gruppo_id);
        $groupable = config('buzzgroups.relationships.'.$groupable_name);
        if($groupable == null) {
            abort(404);
        }

        if ($groupable::apiResource() != null) {
            return ($groupable::apiResource())::collection($gruppo->groupable($groupable_name)->paginate());
        }
        return $gruppo->groupable($groupable_name)->get();
    }

    public function list(Request $request, $gruppo_id)
    {
        $groupable_name = $request->route()->getName();
        $gruppo = $this->repo->show($gruppo_id);
        $groupable = config('buzzgroups.relationships.'.$groupable_name);
        if($groupable == null) {
            abort(404);
        }

        return Lista::collection($gruppo->groupable($groupable_name)->get());
    }
}
