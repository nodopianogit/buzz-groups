<?php
namespace Nodopiano\Buzz\Groups\Repositories;

use Nodopiano\Buzz\Groups\Models\Gruppo;

class EloquentGroupRepository implements GroupRepository
{
    protected $model;

    public function __construct(Gruppo $gruppo)
    {
        $this->model = $gruppo->with('parent');
    }

    public function load($id)
    {
        $this->model = $this->model->findOrFail($id);
    }

    public function show($id)
    {
        $this->load($id);
        return $this->model;
    }

    public function delete($id)
    {
        return $this->model->findOrFail($id)->delete();
    }

    public function create($attributes = [])
    {
        return $this->model->create($attributes);
    }

    public function update($id, $attributes = [])
    {
        $gruppo =  $this->model->findOrFail($id);
        $gruppo->update($attributes);

        foreach (config('buzzgroups.relationships') as $nome => $model) {
            if (isset($attributes[$nome])) {
                $gruppo->syncRelation($nome, $attributes[$nome]);
            }
        }

        return $gruppo;
    }

    public function filter($column = null, $value = null)
    {
        if($column && $value) {
            $this->model = $this->model->where($column, 'LIKE', '%'.$value.'%');
        }

        return $this;
    }

    public function sort($column = null, $desc = false)
    {
        if ($column) {
            $order = ($desc)? 'desc' : 'asc';
            $this->model = $this->model->orderBy($column, $order);
        }
        return $this;
    }

    public function get()
    {
        return $this->model->with('parent')->get();
    }

    public function all()
    {
        return $this->model->with('parent')->get();
    }

    public function paginate($pageSize)
    {
        return $this->model->paginate($pageSize);
    }

    public function list()
    {
        return $this->model->select('id', 'name')->get();
    }

    public function parent($value = null)
    {
        if($value) {
            if($value == 'null') $value = null;
            $this->model = $this->model->where('parent_id', $value);
        }
        return $this;
    }

    public function getRoots()
    {
        return $this->model->where('parent_id', null)->get();
    }

    public function findByName($name)
    {
        return $this->model::whereRaw('LOWER(name) = "'. strtolower($name) . '"')->get();
    }
}
