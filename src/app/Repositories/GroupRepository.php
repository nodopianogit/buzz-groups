<?php
namespace Nodopiano\Buzz\Groups\Repositories;

interface GroupRepository
{
    public function load($id);
    public function show($id);
    public function delete($id);
    public function create($attributes = []);
    public function update($id, $attributes = []);
    public function filter($column = null, $value = null);
    public function sort($column = null, $desc = false);
    public function get();
    public function paginate($pageSize);
    public function list();

    public function parent($value = null);
    public function getRoots();
    public function findByName($name);
}
