<?php

namespace Nodopiano\Buzz\Groups\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Gerarchia extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $gerarchia = (object) array(
                'id' => $this->id,
                'name' => $this->name,
        );

        if ($this->sottogruppi->isEmpty()) {
            return $gerarchia;
        }

        $children = array();
        foreach ($this->sottogruppi as $sottogruppo) {
            $children[] = new self($sottogruppo);
        }

        $gerarchia->children = $children;
        return $gerarchia;
    }
}
