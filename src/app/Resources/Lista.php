<?php

namespace Nodopiano\Buzz\Groups\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Lista extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->getDisplayedName()
        ];
    }
}
