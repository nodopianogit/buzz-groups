<?php

namespace Nodopiano\Buzz\Groups\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Gruppo extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $value = [
             'id' => $this->id,
             'name' => $this->name,
             'parent' => ($this->parent_id == null)? null : new Gruppo($this->parent)
         ];

        foreach (config('buzzgroups.relationships') as $nome => $model) {
            $value[$nome . '_count'] = $this->groupable($nome)->count();
        }

        return $value;
    }
}
